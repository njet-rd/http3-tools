# http3_tools

#### 介绍
在CentOS7下测试http3功能需要的工具，
也可在ubuntu18，ubuntu20, ubuntu22下使用。

#### 软件
1. curl-http3

使用quictls库，具体参数使用`curl-http3 --help all`查看
```
[root@cdn108 bin]# curl-http3 -vs -D/dev/stdout -o/dev/null --http3 https://quic.nginx.org
* processing: https://quic.nginx.org
*   Trying 35.214.218.230:443...
*  CAfile: /etc/pki/tls/certs/ca-bundle.crt
*  CApath: none
*   Trying 35.214.218.230:443...
* Connected to quic.nginx.org (35.214.218.230) port 443
* ALPN: offers http/1.1
} [5 bytes data]
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
} [512 bytes data]
*  subjectAltName: host "quic.nginx.org" matched cert's "quic.nginx.org"
* Verified certificate just fine
* Connected to quic.nginx.org (35.214.218.230) port 443
* using HTTP/3
* Using HTTP/3 Stream ID: 0
> GET / HTTP/3
> Host: quic.nginx.org
> User-Agent: curl/8.2.1-DEV
> Accept: */*
>
< HTTP/3 200
HTTP/3 200
< server: nginx/1.25.2
server: nginx/1.25.2
< date: Mon, 31 Jul 2023 07:45:59 GMT
date: Mon, 31 Jul 2023 07:45:59 GMT
< content-type: text/html
content-type: text/html
< content-length: 2519
content-length: 2519
< last-modified: Wed, 24 May 2023 03:17:46 GMT
last-modified: Wed, 24 May 2023 03:17:46 GMT
< etag: "646d81da-9d7"
etag: "646d81da-9d7"
< alt-svc: h3=":443"; ma=86400
alt-svc: h3=":443"; ma=86400
< x-quic: h3
x-quic: h3
< accept-ranges: bytes
accept-ranges: bytes

<
{ [2519 bytes data]
* Connection #0 to host quic.nginx.org left intact
[root@cdn108 bin]#
```

2. hey

在开源项目[hey](https://github.com/rakyll/hey)，[http_bench](https://github.com/linkxzhou/http_bench), [hey_hdr](https://github.com/asoorm/hey-hdr)基础上开发的 http3压测工具，具体使用参考`hey --help`查看
```
[root@node90 ~]# hey -n 10 -c 5 -h3  https://quic.nginx.org

Summary:
  Total:        5.8759 secs
  Slowest:      3.9820 secs
  Fastest:      0.5170 secs
  Average:      1.7755 secs
  Requests/sec: 1.7019

  Total data:   25190 bytes
  Size/request: 2519 bytes

Response time histogram:
  0.517 [1]     |■■■■■■■■■■■■■
  0.863 [1]     |■■■■■■■■■■■■■
  1.210 [1]     |■■■■■■■■■■■■■
  1.556 [2]     |■■■■■■■■■■■■■■■■■■■■■■■■■■■
  1.903 [3]     |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  2.250 [0]     |
  2.596 [0]     |
  2.943 [0]     |
  3.289 [1]     |■■■■■■■■■■■■■
  3.636 [0]     |
  3.982 [1]     |■■■■■■■■■■■■■


Details (average, fastest, slowest):
  DNS+dialup:   0.0000 secs, 0.0000 secs, 0.0000 secs
  DNS-lookup:   0.0000 secs, 0.0000 secs, 0.0000 secs
  req write:    0.0000 secs, 0.0000 secs, 0.0000 secs
  resp wait:    0.0000 secs, 0.0000 secs, 0.0000 secs
  resp read:    2.7652 secs, 0.8290 secs, 5.8762 secs

Status code distribution:
  [200] 10 responses




  Count: 10
    Max: 3.982079s
   Mean: 1.7755s
    Std: 1.015741s
50.000%: 1.341567s
75.000%: 1.855231s
95.000%: 3.982079s
99.000%: 3.982079s
99.900%: 3.982079s
99.990%: 3.982079s
99.999%: 3.982079s

 Value  Percentile      TotalCount      1/(1-Percentile)

  516975.000     0.000000            1         1.00
  516975.000     0.100000            1         1.11
  790111.000     0.200000            2         1.25
 1070143.000     0.300000            3         1.43
 1341503.000     0.400000            4         1.67
 1341567.000     0.500000            5         2.00
 1832831.000     0.550000            7         2.22
 1832831.000     0.600000            7         2.50
 1832831.000     0.650000            7         2.86
 1832831.000     0.700000            7         3.33
 1855231.000     0.750000            8         4.00
 1855231.000     0.775000            8         4.44
 1855231.000     0.800000            8         5.00
 3192063.000     0.825000            9         5.71
 3192063.000     0.850000            9         6.67
 3192063.000     0.875000            9         8.00
 3192063.000     0.887500            9         8.89
 3192063.000     0.900000            9        10.00
 3982079.000     0.912500           10        11.43
 3982079.000     1.000000           10          inf
#[Mean    =  1775500.000, StdDeviation   =  1015741.350]
#[Max     =  3982079.000, Total count    =           10]
#[Buckets =           25, SubBuckets     =        32768]


```

